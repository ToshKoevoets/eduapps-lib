import React, {Component} from 'react';
import axios from 'axios';
import {Text, View} from 'react-native';

import Content from './Content';
import Title from './Title';
import DisplayImage from './DisplayImage';

class AppDisplayer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            app: false,
            loading: true
        };
    }

    componentDidMount() {
        this.fetchApp()
    }

    fetchApp() {
        axios.get(`${this.props.apiUrl}/api/app/${this.props.appId}`)
            .then((response) => {
                const appResource = response.data;

                this.setState({
                    app: appResource,
                    loading: false
                });

                this.fetchRoutes();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    // fetch routes for screen navigation
    fetchRoutes () {

    }

    render() {
            const components = dummyComponents; //this.props.app.components

            return (
            <>
                <Text> Hello world </Text>
                {components.map((component)=> {
                    if (component.type === 'Title') {
                        return <Title {...component.props} />
                    }
                    if (component.type === 'Image') {
                        return <DisplayImage {...component.props} />
                    }
                    if (component.type === 'Content') {
                        return <Content {...component.props} />
                    }
                })}
            </>
        )
    }
}

const Loader = () => <Text>Loading..</Text>;

export default AppDisplayer;