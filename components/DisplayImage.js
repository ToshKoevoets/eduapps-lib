import React from "react";
import { View, StyleSheet, Image } from "react-native";

const styles = StyleSheet.create({
    titleText: {
        fontSize: 20,
        fontWeight: "bold"
    }
});

const DisplayImage = (props) => {
    return (
        <View style={{
            justifyContent: 'center',
            alignItems: 'center',
        }}>
            <Image source={{uri: props.src}} style={{
                height: props.height ? props.height : 300,
                width: props.width ? props.width : '100%',
            }}  resizeMode={"cover"}

            />
        </View>
    );
};

export default DisplayImage;
