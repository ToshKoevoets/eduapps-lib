import React, { useState } from "react";
import { Text, StyleSheet, View } from "react-native";

const textStyles = {
    text: {
        marginTop: 10,
        marginBottom: 10,
        fontSize: 14
    }
};

const Content = (props) => {
    return (
        <Text style={textStyles.text}>
            {props.content}
        </Text>
    );
};

export default Content;
