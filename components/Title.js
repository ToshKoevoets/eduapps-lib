import React from "react";
import {Text} from "react-native";

const styles = {
    titleText: {
        fontSize: 22,
        marginTop: 15,
        marginBottom: 15
    },
};

const Title = (props) => {
    return (
        <Text style={styles.titleText}>
            {props.title}
        </Text>
    );
};


export default Title;
